# ParaCL

Required programs:

- Cmake version  3.21
- Flex
- Bison


# Install and run
```
> git clone https://gitlab.com/taranov.sasha2004/paracl
> mkdir build
> cd build
```
#### How to run ParaCL?

```
> cmake ..
> make
> ./paracl ../name_of_file

```

# Tests
Required programs:

- Python
- Google tests


### How to run end to end tests?
Run this command from directory with ```paracl``` binary
```
> python tests/e2e/e2e_tests.py

```



---
