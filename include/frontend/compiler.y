%language "c++"
%skeleton "lalr1.cc"
%defines
%define api.value.type variant
%define parse.error    verbose
%locations
%param     {yy::driver_t* driver}

//-----------------------------------------------------------------------------------------

%code requires {

#include "tokens/scope.hpp"
#include "inode/inode.hpp"
#include "tokens/operators.hpp"
#include "tokens/comparator.hpp"
#include "tokens/conjunction.hpp"
#include "tokens/utils.hpp"
#include "tokens/io.hpp"

using namespace paracl;
using namespace backend;

namespace yy { class driver_t; }

}

%code {

#include "frontend/driver.hpp"
#include <iostream>


using namespace paracl;
using namespace backend;


scope_t* current_scope = new scope_t(nullptr);

namespace yy {

parser::token_type yylex(parser::semantic_type* yylval, parser::location_type* loc, driver_t* driver);

}

}

//-----------------------------------------------------------------------------------------

%token
    IF       "if"
    ELSE     "else"
    WHILE    "while"
    OUTPUT   "print"
    INPUT    "?"
    ASSIGN   "="
    SCOLON   ";"
    <std::string> ID
    <int> NUMBER
    MUL      "*"
    PLUS     "+"
    MINUS    "-"
    DIV      "/"
    MODULO   "%"
    OP_PR    "("
    CL_PR    ")"
    EQUAL    "=="
    NOT_EQUAL "!="
    GREATER  ">"
    NEGATIVE "!"
    LESS     "<"
    LS_EQUAL "<="
    GR_EQUAL ">="
    OP_BR    "{"
    CL_BR    "}"

    AND      "&&"
    OR       "||"
    ERR
;

%right ASSIGN
%left  EQUAL NOT_EQUAL GREATER LESS GR_EQUAL LS_EQUAL NEGATIVE
%left  PLUS MINUS OR
%left  MUL DIV MODULO AND
%left INPUT

%left UMINUS

%nterm<paracl::inode_t*>  stms
%nterm<paracl::inode_t*>  stm
%nterm<paracl::inode_t*>  if
%nterm<paracl::inode_t*>  act_body
%nterm<paracl::inode_t*>  while
%nterm<paracl::inode_t*>  oper
%nterm<paracl::inode_t*>  comp
%nterm<paracl::inode_t*>  block
%nterm<paracl::inode_t*>  no_cond_stm

%nterm<paracl::inode_t*>  condition
%nterm<paracl::inode_t*>  expr


%nterm<paracl::inode_t*> assign
%nterm<std::string> lvalue
%nterm<paracl::inode_t*> output
%nterm<paracl::inode_t*> input

%nterm<backend::scope_t*> open_sc
%nterm<backend::scope_t*> close_sc

//-----------------------------------------------------------------------------------------

%start program


%%

program: %empty
        | stms { if (driver->compil_err_flag) {
                    std::cout << "No compilation\n";
                    return driver->print_errors();
                }
                current_scope->execute(*current_scope);};


stms: stms stm                     {current_scope->insert($2);}
     | stms open_sc block close_sc {current_scope->insert($2);} //shift reduce
     | open_sc block close_sc      {current_scope->insert($1);}
     | stm                         {current_scope->insert($1);}
;

stm:  oper           {$$ = $1;}
    | no_cond_stm    {$$ = $1;}
    | error OP_BR    {$$ = backend::create_error();}
    | error SCOLON   {$$ = backend::create_error();}
    | error          {$$ = backend::create_error();}
;

no_cond_stm:
      expr   SCOLON  {$$ = $1;}
    | output SCOLON  {$$ = $1;}
    | SCOLON         {$$ = create_empty();} //one shift/reduce

// ------------------------SCOPE_INSTRUCTIONS-----------------------------------------------

block: OP_BR stms CL_BR {$$ = $2;}
     | OP_BR CL_BR {$$ = backend::create_empty();};
    //no_cond_stm {current_scope->insert($1);}


open_sc:  %empty   {$$ = backend::create_scope(current_scope); current_scope = $$;}

close_sc: %empty {current_scope = current_scope->upper_scope;}

//------------------------OPERATORS_INSTRUCTIONS-------------------------------------------

oper: if            {$$ = $1;}
    | while         {$$ = $1;}
;

if:  IF condition  open_sc  act_body close_sc           {$$ = backend::create_if($2, $3);};
    |IF condition  open_sc  act_body close_sc ELSE oper {$$ = backend::create_if($2, $3, $7);}
    |IF condition  open_sc  act_body close_sc ELSE
                   open_sc  no_cond_stm {current_scope->insert($8);} close_sc {$$ = backend::create_if($2, $3, $7);}
    |IF condition  open_sc  act_body close_sc ELSE open_sc  block close_sc {$$ = backend::create_if($2, $3, $7);}
;
while: WHILE condition  open_sc act_body  close_sc  {$$ = backend::create_while($2, $3);};
;

act_body: block                     {$$ = $1;}
        | stm                       {current_scope->insert($1);}
;

//------------------------ASSIGNMENT_INSTRUCTIONS------------------------------------------

assign: lvalue ASSIGN expr {auto var = backend::create_var(0, $1);
                             current_scope->insert(var);
                             $$ = new assign_t(var, $3);
                           };

lvalue: ID                 {$$ = $1;};

//------------------------EXPRESSIONS_INSTRUCTIONS-----------------------------------------

condition: OP_PR expr CL_PR {$$ = $2;};
         | OP_PR output CL_PR {$$ = $2;} // not beautiful

;

comp: expr EQUAL     expr {$$ = new equal_t($1, $3);}
    | expr NOT_EQUAL expr {$$ = new not_equal_t($1,$3);}
    | expr GREATER   expr {$$ = new greater_t($1, $3);}
    | expr LESS      expr {$$ = new less_t($1, $3);}
    | expr LS_EQUAL  expr {$$ = new ls_equal_t($1, $3);}
    | expr GR_EQUAL  expr {$$ = new gr_equal_t($1, $3);}
    | expr AND       expr {$$ = new and_t($1, $3);}
    | expr OR        expr {$$ = new or_t($1, $3);}
;

expr: NUMBER           {$$ = backend::create_number($1);}
    | ID               {$$ = backend::create_name($1); current_scope->insert($$);}
    | expr PLUS   expr {$$ = new plus_t($1, $3);}
    | expr MINUS  expr {$$ = new minus_t($1, $3);}
    | expr MUL    expr {$$ = new mul_t($1, $3);}
    | expr DIV    expr {$$ = new backend::div_t($1, $3);}
    | expr MODULO expr {$$ = new modulo_t($1, $3);}
    | OP_PR expr CL_PR {$$ = $2;}
    | comp             {$$ = $1;}
    | assign           {$$ = $1;}
    | input            {$$ = $1;}
    | MINUS expr %prec UMINUS {$$ = new mul_t(backend::create_number(-1), $2);}
    | NEGATIVE expr {$$ = new negative_t($2);}
;

//------------------------IO_INSTRUCTIONS--------------------------------------------------

output: OUTPUT expr {$$ = new output_t($2);}
input : INPUT       {$$ = new input_t(nullptr);}

//-----------------------------------------------------------------------------------------

%%

namespace yy {

parser::token_type yylex(parser::semantic_type* yylval, parser::location_type* loc, driver_t* driver) {
    return driver->yylex(yylval, loc);
}

void parser::error(const parser::location_type& loc, const std::string& msg)
{
    driver->error(loc, msg);
}

}
