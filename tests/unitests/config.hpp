#pragma once

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "tokens/operators.hpp"
#include "tokens/comparator.hpp"
#include "tokens/cond_operators.hpp"
#include "inode/inode.hpp"
